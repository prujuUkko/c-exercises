#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gameoflife.h"

Field *createField(unsigned int xsize, unsigned int ysize) {
	Field *fld = malloc(sizeof(Field));
	fld->cells = malloc(sizeof(State*) * ysize);

	for (int j = 0; j < ysize; j++) {
		fld->cells[j] = malloc(xsize * sizeof(State));

		for (int i = 0; i < xsize; i++) {
			fld->cells[j][i] = 0;
		}
	}
	fld->xsize = xsize;
	fld->ysize = ysize;
	return fld;
}

void releaseField(Field *f) {
	for (int j = 0; j < f->ysize; j++) {
		free(f->cells[j]);
	}
	free(f->cells);
	free(f);
}

void initField(Field *f, unsigned int n) {
	int k = 0;
	int j = 0;
	int i = 0;
	for (j = 0; j < f->ysize; j++) {
    if (k == n) {
			break;
		}
		for (i = 0; i < f->xsize; i++) {
			if (k == n) {
			  break;
		  }
			f->cells[j][i] = 1;
			k++;
		}

	}


}

void printField(const Field *f) {
	int i, j;
	for (j = 0; j < f->ysize; j++) {
		for (i = 0; i < f->xsize; i++) {
			if (f->cells[j][i] == 0) {
				printf(".");
			}

			if (f->cells[j][i] == 1) {
				printf("*");
			}
		}
		printf("\n");
	}
}


void tick(Field *f) {
	Field *tmp = createField(f->xsize, f->ysize);
	for (int j = 0; j < tmp->ysize; j++) {
		for (int i = 0; i < tmp->xsize; i++) {
			tmp->cells[j][i] = f->cells[j][i];
		}
	}

	for (int j = 0; j < tmp->ysize; j++) {
		for (int i = 0; i < tmp->xsize; i++) {
				int count = 0;
				if (j == 0 && i != 0 && i != tmp->xsize - 1) {
					if (tmp->cells[j][i + 1] == 1) {
						count++;
					}
					if (tmp->cells[j][i - 1] == 1) {
						count++;
					}
					if (tmp->cells[j + 1][i] == 1) {
						count++;
					}
					if (tmp->cells[j + 1][i - 1] == 1) {
						count++;
					}
					if (tmp->cells[j + 1][i + 1] == 1) {
						count++;
					}
				}

				else if (j == tmp->ysize - 1 && i != 0 && i != tmp->xsize - 1) {
					if (tmp->cells[j][i + 1] == 1) {
						count++;
					}
					if (tmp->cells[j][i - 1] == 1) {
						count++;
					}
					if (tmp->cells[j - 1][i] == 1) {
						count++;
					}
					if (tmp->cells[j - 1][i + 1] == 1) {
						count++;
					}
					if (tmp->cells[j - 1][i - 1] == 1) {
						count++;
					}
				}

				else if (i == 0 && j != 0 && j != tmp->ysize - 1) {
					if (tmp->cells[j][i + 1] == 1) {
						count++;
					}
				  if (tmp->cells[j + 1][i] == 1) {
					  count++;
					}
					if (tmp->cells[j + 1][i + 1] == 1) {
						count++;
					}
					if (tmp->cells[j - 1][i] == 1) {
						count++;
					}
					if (tmp->cells[j - 1][i + 1] == 1) {
						count++;
					}
				}

				else if (i == tmp->xsize - 1 && j != 0 && j != tmp->ysize - 1) {
					if (tmp->cells[j][i - 1] == 1) {
						count++;
					}
					if (tmp->cells[j + 1][i] == 1) {
						count++;
					}
					if (tmp->cells[j + 1][i - 1] == 1) {
						count++;
					}
					if (tmp->cells[j - 1][i] == 1) {
						count++;
					}
					if (tmp->cells[j - 1][i - 1] == 1) {
						count++;
					}

				}

				else if (j == 0 && i == 0) {
					if (tmp->cells[j][i + 1] == 1) {
						count++;
					}
					if (tmp->cells[j + 1][i] == 1) {
						count++;
					}
					if (tmp->cells[j + 1][i + 1] == 1) {
						count++;
					}
				}

				else if (j == 0 && i == tmp->xsize - 1) {
					if (tmp->cells[j][i - 1] == 1) {
						count++;
					}
					if (tmp->cells[j + 1][i] == 1) {
						count++;
					}
					if (tmp->cells[j + 1][i - 1] == 1) {
						count++;
					}
				}

				else if (j == tmp->ysize - 1 && i == 0) {
					if (tmp->cells[j][i + 1] == 1) {
						count++;
					}
			        if (tmp->cells[j - 1][i] == 1) {
						count++;
					}
					if (tmp->cells[j - 1][i + 1] == 1) {
						count++;
					}
				}

				else if (j == tmp->ysize - 1 && i == tmp->xsize - 1) {

					if (tmp->cells[j][i - 1] == 1) {
						count++;
					}
					if (tmp->cells[j - 1][i] == 1) {
						count++;
					}
					if (tmp->cells[j - 1][i - 1] == 1) {
						count++;
					}
                }

				else {
					if (tmp->cells[j][i + 1] == 1) {
						count++;
					}
					if (tmp->cells[j][i - 1] == 1) {
						count++;
					}
					if (tmp->cells[j + 1][i] == 1) {
						count++;
					}
					if (tmp->cells[j + 1][i - 1] == 1) {
						count++;
					}
					if (tmp->cells[j + 1][i + 1] == 1) {
						count++;
					}
					if (tmp->cells[j - 1][i] == 1) {
						count++;
					}
					if (tmp->cells[j - 1][i + 1] == 1) {
						count++;
					}
					if (tmp->cells[j - 1][i - 1] == 1) {
						count++;
					}
        }
       if (tmp->cells[j][i] == 0) {
				 if (count == 3) {
					 f->cells[j][i] = 1;
				 }
			 }
			 if (tmp->cells[j][i] == 1) {
				 if (count < 2) {
					 f->cells[j][i] = 0;
 				 }
				 if (count == 2) {
					 f->cells[j][i] = 1;
				 }
				 if (count == 3) {
					 f->cells[j][i] = 1;
				 }
				 if (count > 3) {
					 f->cells[j][i] = 0;
				 }
			 }
			}
		}
		releaseField(tmp);
}
