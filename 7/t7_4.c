#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "strarray.h"

/* Exercise a: Initializes the string array to contain the initial
* NULL pointer, but nothing else.
* Returns: pointer to the array of strings that has one element
*      (that contains NULL)
*/
char **init_array(void)
{
	char **arr = malloc(sizeof(char *));
	arr[0] = NULL;
	return arr;
}

/* Releases the memory used by the strings.
*/
void free_strings(char **array)
{
	int k = 1;
	int len = 0;
	while (k == 1) {
		if (array[len] == NULL) {
			break;
		}
		len++;
	}
	for (int j = 0; j < len; j++) {
		free(array[j]);
	}
	free(array);
}

/* Exercise b: Add <string> to the end of array <array>.
* Returns: pointer to the array after the string has been added.
*/
char **add_string(char **array, const char *string)
{
	int i = 0;
	int j = 1;
	while (j == 1) {
		if (array[i] == NULL) {
			break;
		}
		i++;
	}

	array = realloc(array, i * sizeof(char *) + 2 * sizeof(char *));
	array[i] = malloc(strlen(string)+1);
	strcpy(array[i], string);
	array[i + 1] = NULL;
	return array;
}


/* Exercise c: reorder strings in <array> to lexicographical order */
void sort_strings(char **array)
{
	int len = 0;
	int k = 1;
	while (k == 1) {
		if (array[len] == NULL) {
			break;
		}
		len++;
	}
	int i, j;

	for (i = 1; i < len; i++) {
		for (j = 1; j < len; j++) {
			if (strcmp(array[j - 1], array[j]) > 0) {
				char *tmp;
				tmp = array[j - 1];
				array[j - 1] = array[j];
				array[j] = tmp;
			}
		}
	}
}

/* You can use this function to check what your array looks like.
*/
void print_strings(char **array)
{
	if (!array)
		return;
	while (*array) {
		printf("%s  ", *array);
		array++;
	}
	printf("\n");
}
