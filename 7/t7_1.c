#include <stdio.h>
#include <string.h>
#include "anydata.h"


/* 01_anydata (a)
* Return a new AnyData object based on the given input
*/
AnyData setDouble(double value)
{
	AnyData asd;
	asd.type = DOUBLE;
	asd.value.dval = value;
	return asd;
}

AnyData setInt(int value)
{
	AnyData asd;
	asd.type = INT;
	asd.value.ival = value;
	return asd;
}

AnyData setString(char* value)
{
	AnyData asd;
	asd.type = STRING;
	strcpy(asd.value.sval, value);
	return asd;
}


void printDouble(double val)
{
	printf("D:%lf", val);
}

void printInt(int val)
{
	printf("I:%d", val);
}

void printString(char* val)
{
	printf("S:%s", val);
}

/* 01_anydata (b)
* Print the given AnyData value, using one of the above functions
*/
void printValue(AnyData val)
{
	if (val.type == DOUBLE) {
		printDouble(val.value.dval);
   }
	if (val.type == INT) {
		printInt(val.value.ival);
	}
	if (val.type == STRING) {
		printString(val.value.sval);
	}
}
