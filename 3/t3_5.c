#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>


void mastermind(const int *solution, const int *guess, char *result, unsigned int len) {
  int i;
  char a = '+' ;
  char b = '*' ;
  char c = '-' ;
  char *ptr_a = &a;
  char *ptr_b = &b;
  char *ptr_c = &c;

  for (i = 0; i<len; i++) {

    int j;
    int d;

    for (j = 0; j < len; j++) {

      if (solution[j] == guess[i]) {
        d = 1;
        break;
      }

      if (solution[j] != i) {
        d = 0;
      }
    }


    if (d == 1) {

      if (solution[i] == guess[i]) {
        result[i] = *ptr_a;
      }
      if (solution[i] != guess[i]) {
        result[i] = *ptr_b;
      }
    }
    if (d == 0) {
      result[i] = *ptr_c;
    }
  }
}

int main()
{
    /* 03-mastermind */
    int sol[6], guess[6];
    char result[7];
    srand((unsigned) time(NULL));
    for (int i = 0; i < 6; i++) {
        sol[i] = rand() % 10;
        // uncomment below, if you want to cheat (or test)
        printf("%d ", sol[i]);
    }
    unsigned int attempts = 10;
    do {
        printf("\nAttempts remaining: %d -- give 6 numbers: ", attempts);
        for (int i = 0; i < 6; i++)
            scanf("%d", &guess[i]);
        mastermind(sol, guess, result, 6);
        result[6] = 0;
        printf("  Result: %s", result);
    } while (attempts-- && strcmp(result, "++++++"));

    return 0;
}
