#include <stdio.h>
#include <arpa/inet.h>
#include "ipheader.h"


/* Parses the given buffer into an IP header structure.
*
* Parameters:
* ip: pointer to the IP header structure that will be filled based
*      on the data in the buffer
* buffer: buffer of 20 bytes that contain the IP header. */
void parseIp(struct ipHeader *ip, const void *buffer)
{
	unsigned char* cbuf = (unsigned char*)buffer;
	//version
	ip->version = cbuf[0] >> 4;
	//ihl
	unsigned char temp1 = cbuf[0] << 4;
	temp1 = temp1 >> 4;
	ip->ihl = temp1*4;
	//dscp
	ip->dscp = cbuf[1] >> 2;
	//ecn
	temp1 = cbuf[1] << 6;
	temp1 = temp1 >> 6;
	ip->ecn = temp1;
	//length
	ip->length = (cbuf[2] << 8) | cbuf[3];
	//id
	ip->identification = (cbuf[4] << 8) | cbuf[5];
	//flags
	ip->flags = cbuf[6] >> 5;
	//offset
	unsigned char temp2 = cbuf[6] << 3;
	temp2 = temp2 >> 3;
	ip->fragment_offset = (temp2 << 8) | cbuf[7];
	//time to live
	ip->time_to_live = cbuf[8];
	//protocol
	ip->protocol = cbuf[9];
	//header checksum
	ip->header_checksum = (cbuf[10] << 8) | cbuf[11];
	//source ip
	ip->source_ip[0] = cbuf[12];
	ip->source_ip[1] = cbuf[13];
	ip->source_ip[2] = cbuf[14];
	ip->source_ip[3] = cbuf[15];
	//destination ip
	ip->destination_ip[0] = cbuf[16];
	ip->destination_ip[1] = cbuf[17];
	ip->destination_ip[2] = cbuf[18];
	ip->destination_ip[3] = cbuf[19];
}


/* Builds a 20-byte byte stream based on the given IP header structure
*
* Parameters:
* buffer: pointer to the 20-byte buffer to which the header is constructed
* ip: IP header structure that will be packed to the buffer */
void sendIp(void *buffer, const struct ipHeader *ip)
{
	unsigned char* cbuf = (unsigned char*)buffer;
	//version
	cbuf[0] = cbuf[0] | ip->version;
	//ihl
	cbuf[0] = cbuf[0] << 4;
	cbuf[0] = cbuf[0] | ((ip->ihl) / 4);
	//dscp
	cbuf[1] = cbuf[1] | ip->dscp;
	//ecn
	cbuf[1] = cbuf[1] << 2;
	cbuf[1] = cbuf[1] | ip->ecn;
	//length
	unsigned short temp1 = ip->length & 0xff;
	unsigned short temp2 = ip->length & 0xff00;
	temp2 = temp2 >> 8;
	cbuf[2] = cbuf[2] | temp2;
	cbuf[3] = cbuf[3] | temp1;
	//id
	temp1 = ip->identification & 0xff;
	temp2 = ip->identification & 0xff00;
	temp2 = temp2 >> 8;
	cbuf[4] = cbuf[4] | temp2;
	cbuf[5] = cbuf[5] | temp1;
	//flags
	cbuf[6] = cbuf[6] | ip->flags << 5;
	//offset
	temp1 = ip->fragment_offset & 0xff;
	temp2 = ip->fragment_offset & 0xff00;
	temp2 = temp2 >> 8;
	cbuf[6] = cbuf[6] | temp2;
	cbuf[7] = cbuf[7] | temp1;
	//time to live
	cbuf[8] = cbuf[8] | ip->time_to_live;
	//protocol
	cbuf[9] = cbuf[9] | ip->protocol;
	//header checksum
	temp1 = ip->header_checksum & 0xff;
	temp2 = ip->header_checksum & 0xff00;
	temp2 = temp2 >> 8;
	cbuf[10] = cbuf[10] | temp2;
	cbuf[11] = cbuf[11] | temp1;
	//source ip
	cbuf[12] = cbuf[12] | ip->source_ip[0];
	cbuf[13] = cbuf[13] | ip->source_ip[1];
	cbuf[14] = cbuf[14] | ip->source_ip[2];
	cbuf[15] = cbuf[15] | ip->source_ip[3];
	//destination ip
	cbuf[16] = cbuf[16] | ip->destination_ip[0];
	cbuf[17] = cbuf[17] | ip->destination_ip[1];
	cbuf[18] = cbuf[18] | ip->destination_ip[2];
	cbuf[19] = cbuf[19] | ip->destination_ip[3];

	buffer = cbuf;
}


/* Prints the given IP header structure */
void printIp(const struct ipHeader *ip)
{
	/* Note: ntohs below is for converting numbers from network byte order
	to host byte order. Subject of later courses, you can ignore them */
	printf("version: %d   ihl: %d   dscp: %d   ecn: %d\n",
		ip->version, ip->ihl, ip->dscp, ip->ecn);
	printf("length: %d   id: %d   flags: %d   offset: %d\n",
		ntohs(ip->length), ntohs(ip->identification), ip->flags, ip->fragment_offset);
	printf("time to live: %d   protocol: %d   checksum: 0x%04x\n",
		ip->time_to_live, ip->protocol, ntohs(ip->header_checksum));
	printf("source ip: %d.%d.%d.%d\n", ip->source_ip[0], ip->source_ip[1],
		ip->source_ip[2], ip->source_ip[3]);
	printf("destination ip: %d.%d.%d.%d\n", ip->destination_ip[0],
		ip->destination_ip[1],
		ip->destination_ip[2], ip->destination_ip[3]);
}

/* Shows hexdump of given data buffer */
void hexdump(const void *buffer, unsigned int length)
{
	const unsigned char *cbuf = buffer;
	unsigned int i;
	for (i = 0; i < length; ) {
		printf("%02x ", cbuf[i]);
		i++;
		if (!(i % 8))
			printf("\n");
	}
}
