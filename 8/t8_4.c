#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include "bits.h"

void bit_unset(unsigned char* data, int idx) {
	int i;
	int j;
	if (idx > 8) {
		i = idx / 8;
		j = 7 - (idx - (i * 8));
	}

	else {
		i = 0;
		j = 7 - idx;
	}

	data[i] &= ~(1 << j);
}

void bit_set(unsigned char* data, int idx) {
	int i;
	int j;
	if (idx > 8) {
		i = idx / 8;
		j = 7 - (idx - (i * 8));
	}

	else {
		i = 0;
		j = 7 - idx;
	}

	data[i] |= 1 << j;
}

int bit_get(const unsigned char* data, int idx) {
	int ret;
	int i;
	int j;
	if (idx > 8) {
		i = idx / 8;
		j = 7 - (idx - (i * 8));
	}

	else {
		i = 0;
		j = 7 -  idx;
	}
	ret = (data[i] >> j) & 1;
	return ret;
}

void print_byte(unsigned char b) {
	int asd[8] = { 0 };
	int i = 0;
	while (i < 8) {
		asd[7 - i] = (b >> i) & 1;
		i++;
	}
	int j = 0;
	while (j < 8) {
		printf("%d", asd[j]);
		j++;
	}
}

unsigned char bit_get_sequence(const unsigned char* data, int idx, int how_many) {
	unsigned char asd[1] = { 0 };
	int i = 0;
	int abc = 0;
	while (i < how_many) {
		abc = bit_get(data, idx + i);
		if (abc == 1) {
			bit_set(asd, i);
		}

		if (abc == 0) {
			bit_unset(asd, i);
		}
		printf("%d", abc);
		i++;
	}
	printf("\n");
	print_byte(asd[1]);
	printf("\n");
	asd[0] = asd[0] >> (8 - how_many);
	return asd[0];
}
