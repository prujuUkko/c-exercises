#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "filebasics.h"

int print_file(const char *filename) {
	int c;
	int ret = 0;
	FILE *f = fopen(filename, "r");
	if (!f) {
		ret = -1;
	}

	if (f) {
		while ((c = getc(f)) != EOF) {
			putchar(c);
			ret++;
		}
		fclose(f);

	}
	return ret;
}

char *difference(const char* file1, const char* file2) {
	FILE *f1 = fopen(file1, "r");
	FILE *f2 = fopen(file2, "r");
	char *str1 = malloc(1000);
	char *str2 = malloc(1000);
	char *ret = malloc(1000);
	//ret = NULL;
	int cmp = 0;

	while (fgets(str1, 1000, f1) && fgets(str2, 1000, f2)) {
		cmp = strcmp(str1, str2);
		if (cmp != 0) {
			strcat(str1, "----\n");
			strcat(str1, str2);

			strcpy(ret,str1);
			free(str1);
			free(str2);
			break;

		}
	}
	if (cmp == 0) {
		ret = NULL;
	}
	return ret;
}
