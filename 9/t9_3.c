#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "filestats.h"

int line_count(const char *filename) {
	FILE *f = fopen(filename, "r");
	unsigned char c;
	int count = 0;

	if (!f) {
		count = -1;
	}

	if (f) {
		while (feof(f) == 0) {
			c = fgetc(f);

			if (c == '\n') {
				count++;
			}
		}

		fseek(f, -1, SEEK_END);
		c = fgetc(f);

		if (c != '\n' && count != 0) {
			count++;
		}

		fseek(f, 0, SEEK_END);
		long i = ftell(f);

		if (i == 0) {
			count = 0;
        }

		if (i != 0 && count == 0) {
			count++;
		}
	}

	return count;
}

int word_count(const char *filename) {
	int count = 0;
	FILE *f = fopen(filename, "r");
	unsigned char c = ' ';
	unsigned char e = ' ';

	if (!f) {
		count = -1;
	}

	if (f) {
		while (feof(f) == 0) {
			c = fgetc(f);

			if (isalpha(c)) {
				e = c;
			}

			if (isalpha(e) && isspace(c)) {
				count++;
				e = ' ';
			}

		}
		if (isalpha(e) && feof(f) != 0) {
			count++;
		}
	}

	return count;
}
