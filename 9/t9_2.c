#include <stdio.h>
#include <string.h>

int hexdump(const char *filename) {
	int ret = 0;
	int i = 0;
	FILE *f = fopen(filename, "r");
	unsigned char c;
	if (!f) {
		ret = -1;
	}

	if (f) {
		while (feof(f) == 0) {

			if (i % 16 == 0 && i != 0) {
				c = fgetc(f);
				if (c != 0xff) {
					printf("\n");
					printf("%.02x ", c);
					i++;
				}

			}

			else {
				c = fgetc(f);
				if (c != 0xff) {
					printf("%.02x ", c);
					i++;
				}

			}

			ret = ftell(f);
		}
	}

	return ret;
}
