#include "election.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct votes *read_votes(const char *filename)
{
	FILE *f = fopen(filename, "r");

	struct votes *ret = malloc(sizeof(struct votes));
	strcpy(ret[0].name, " ");
	ret[0].votes = 0;

	char c[40];
	int array_size;
	int i;
	int in_array;
	while (feof(f) == 0) {

		fgets(c, 40, f);
		c[strcspn(c, "\r\n")] = 0;


		array_size = 1;
		while (strcmp(ret[array_size - 1].name, " ") != 0) {
			array_size++;
		}


		i = 0;
		in_array = 0;
        for (i = 0; i < array_size; i++) {
			if (strcmp(c, ret[i].name) == 0) {
				in_array = 1;
				break;
			}
		}


		if (in_array == 0) {
			ret = realloc(ret, array_size * sizeof(struct votes) + sizeof(struct votes));
			strcpy(ret[array_size].name, " ");
			ret[array_size].votes = 0;
			strncpy(ret[array_size-1].name, c, 40);
			ret[array_size-1].votes = 1;
		}

		if (in_array != 0) {
			ret[i].votes = ret[i].votes + 1;
		}

    }

	int a = 0;
    for (a = 0; a < array_size; a++) {
		if (strcmp(ret[a].name, c) == 0) {
			ret[a].votes = ret[a].votes - 1;
			break;
		}
	}

	return ret;
}

void results(struct votes *vlist) {
	int i, j;
	struct votes tmp;

	int array_size = 1;

	while (strcmp(vlist[array_size - 1].name, " ") != 0) {
		if (strcmp(vlist[array_size - 1].name, " ") == 0) {
			break;
		}
		array_size++;
	}

	for (i = 1; i < array_size; i++) {
		for (j = 1; j < array_size; j++) {
			if (strcmp(vlist[j - 1].name, vlist[j].name) < 0) {
				strcpy(tmp.name, vlist[j - 1].name);
				tmp.votes = vlist[j - 1].votes;
				strcpy(vlist[j - 1].name, vlist[j].name);
				vlist[j - 1].votes = vlist[j].votes;
				strcpy(vlist[j].name, tmp.name);
				vlist[j].votes = tmp.votes;
			}
		}
	}

	for (i = 1; i < array_size; i++) {
		for (j = 1; j < array_size; j++) {
			if (vlist[j - 1].votes > vlist[j].votes) {
				strcpy(tmp.name, vlist[j - 1].name);
				tmp.votes = vlist[j - 1].votes;
				strcpy(vlist[j - 1].name, vlist[j].name);
				vlist[j - 1].votes = vlist[j].votes;
				strcpy(vlist[j].name, tmp.name);
				vlist[j].votes = tmp.votes;
			}
		}
	}

	for (i = array_size - 1; i > 0; i--) {
		printf("%s: %d\n", vlist[i].name, vlist[i].votes);
	}
}
