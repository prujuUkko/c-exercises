#include <stdio.h>
#include <ctype.h>

int count_alpha(const char *str) {
  int ret = 0;
  while (*str) {
    int a = isalpha(*str);
    if (a != 0) {
      ret++;
      str++;
    }
    if (a == 0) {
      str++;
    }
  }
  return ret;
}

void main() {
  const char str[] = "This is a string";
  int b = count_alpha(str);
  printf("%d\n", b);
}
