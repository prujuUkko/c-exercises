#include <stdio.h>

void es_print(const char *s) {
	int i = 0;
	while (s[i] != '#') {
		printf("%c", s[i]);
		i++;
	}
}

unsigned int es_length(const char *s) {
	int ret = 0;
	int i = 0;

	while (s[i] != '#') {
		ret++;
		i++;
	}

	return ret;
}

int es_copy(char *dst, const char *src) {
	int ret = 0;
	int i = 0;

	while (src[i] != '#') {
		dst[i] = src[i];
		ret++;
		i++;
	}
	dst[i] = '#';
	return ret;
}

char *es_token(char *s, char c) {
	int i = 0;
	int k = 0;

	for (i = 0; i < es_length(s);i++){
		if (s[i] == c) {
			k = k + 1;
			break;
		}
	}

	if (k == 1) {
		s[i] = '#';
		int j = 0;
		while (j <= i) {
			s++;
			j++;
		}

		return s;
	}

	else
		return NULL;


}

void main() {
	es_print("aaa,bbb,ccc#ddd,eee");
	unsigned int length = es_length("aaa,bbb,ccc#ddd,eee");
	printf("%d\n", length);
	const char *car = "aaa,bbb,ccc#ddd,eee";
	char asd[11];
	int copy_lenght = es_copy(asd, car);
	printf("%d\n", copy_lenght);
	es_print(asd);
	char *str = "aaa,bbb,ccc#ddd,eee";
	es_token(str, ',');

}
