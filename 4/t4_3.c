#include <stdio.h>
#include <string.h>

int count_substr(const char *str, const char *sub) {
	int ret = 0;
	size_t i = 0;
	size_t asd = strlen(str);

	while (i < asd) {
		char *tmp = strstr(str, sub);
		if (str == tmp) {
			ret++;
		}

		str++;
		i++;
	}
	return ret;
}

int main() {
	int asd = count_substr("one two one twotwo three", "two");
	printf("%d\n", asd);
	return 0;
}
