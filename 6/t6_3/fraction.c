#include <stdlib.h>
#include "fraction.h"

/* Algorithm for determining greatest common divisor, needed in (d) */
/* The function returns gcd between the two parameters, u and v */
/* Taken from http://en.wikipedia.org/wiki/Binary_GCD_algorithm */


unsigned int gcd(unsigned int u, unsigned int v)
{
	// simple cases (termination)
	if (u == v)
		return u;

	if (u == 0)
		return v;

	if (v == 0)
		return u;

	// look for factors of 2
	if (~u & 1) // u is even
	{
		if (v & 1) // v is odd
			return gcd(u >> 1, v);
		else // both u and v are even
			return gcd(u >> 1, v >> 1) << 1;
	}

	if (~v & 1) // u is odd, v is even
		return gcd(u, v >> 1);

	// reduce larger argument
	if (u > v)
		return gcd((u - v) >> 1, v);

	return gcd((v - u) >> 1, u);
}

Fraction* setFraction(unsigned int numerator, unsigned int denominator) {
	Fraction *ret = malloc(sizeof(Fraction));
	ret->numerator = numerator;
	ret->denominator = denominator;
	return ret;
}

void freeFraction(Fraction* f) {
	free(f);
}

unsigned int getNum(const Fraction *f) {
	return f->numerator;
}

unsigned int getDenom(const Fraction *f) {
	return f->denominator;
}

int compFraction(const Fraction *a, const Fraction *b) {
	int ret;

	double a_1;
	double a_2;
	double b_1;
	double b_2;

	a_1 = (double) a->numerator;
	a_2 = (double) a->denominator;
	b_1 = (double) b->numerator;
	b_2 = (double) b->denominator;

    if (a_1 / a_2 > b_1 / b_2) {
		ret = 1;
	}

	if (a_1 / a_2 == b_1 / b_2) {
		ret = 0;
	}

	if (a_1 / a_2 < b_1 / b_2) {
		ret = -1;
	}
	return ret;
}

Fraction *addFraction(const Fraction *a, const Fraction *b) {
	int a_1 = a->numerator * b->denominator;
	int a_2 = a->denominator * b->denominator;
	int b_1 = b->numerator * a->denominator;

	int add_num = a_1 + b_1;
	int add_den = a_2;

	Fraction *ret;
	ret = malloc(sizeof(Fraction));
	ret->numerator = add_num;
	ret->denominator = add_den;

	return ret;
}

void reduceFraction(Fraction *val) {
	unsigned int i = gcd(val->numerator, val->denominator);
	val->numerator = val->numerator / i;
	val->denominator = val->denominator / i;
}
