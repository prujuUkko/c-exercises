#include <string.h>
#include <stdio.h>
#include "source.h"

struct vessel create_vessel(const char *name, double length, double depth, struct cargo crg) {
	struct vessel vessel;
	strncpy(vessel.name, name, 31);
	vessel.name[30] = '\0';
	vessel.length = length;
	vessel.depth = depth;
	vessel.crg = crg;
	return vessel;
}

void print_vessel(const struct vessel *ship) {
	printf("%s\n%.1f\n%.1f\n%s\n%d\n%.1f\n", ship->name, ship->length, ship->depth, ship->crg.title, ship->crg.quantity, ship->crg.weight);
}
