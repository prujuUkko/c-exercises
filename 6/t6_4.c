#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "queue.h"
#include "queuepriv.h"

Queue *Queue_init(void)
{
	Queue *q = calloc(1, sizeof(Queue));
	return q;
}

int Queue_enqueue(Queue *q, const char *id, const char *name)
{
	if (strlen(id) != 6) {
		return 0;
	}
	if (q->last == NULL || q->first == NULL) {
		q->last = malloc(sizeof(struct student));
		q->first = q->last;
		strcpy(q->last->id, id);
		q->last->name = malloc(strlen(name) + 1);
		strncpy(q->last->name, name, strlen(name));
		q->last->name[strlen(name)] = '\0';
		q->first->next = NULL;
	}
	else {
		q->last->next = malloc(sizeof(struct student));
		q->last = q->last->next;
		strcpy(q->last->id, id);
		q->last->name = malloc(strlen(name) + 1);
		strncpy(q->last->name, name, strlen(name));
		q->last->name[strlen(name)] = '\0';
		q->last->next = NULL;
	}

	return 1;
}

char *Queue_firstID(Queue *q)
{
	if (q && q->first)
		return q->first->id;
	else
		return NULL;
}

char *Queue_firstName(Queue *q)
{
	if (q && q->first)
		return q->first->name;
	else
		return NULL;
}

int Queue_dequeue(Queue *q)
{
	if (q->first == NULL) {
		return 0;
	}
	struct student *a = q->first->next;
	free(q->first->name);
	free(q->first);
	q->first = a;


	if (q->first == NULL) {
		q->last = NULL;
	}
	return 1;
}

int Queue_drop(Queue *q, const char *id)
{
	if (q->first == NULL) {
		return 0;
	}

	struct student *a = q->first;
	struct student *b;

	while (strcmp(a->id, id) != 0) {
	    if (a->next == NULL) {
			return 0;
		}
		b = a;
		a = a->next;
	 }

	if (a == q->first) {
		Queue_dequeue(q);
		//free(a->name);
		//free(a);
		return 1;
	}


	if (a == q->last) {
		q->last = b;
	}

	b->next = a->next;
	free(a->name);
	free(a);
    return 1;
}

void Queue_delete(Queue *q)
{
	if (q) {
		while (Queue_dequeue(q));
		free(q);
	}
}
