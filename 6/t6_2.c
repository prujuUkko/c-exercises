#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include "oodi.h"

int init_record(struct oodi * or , const char *p_student, const char *p_course, unsigned char p_grade, struct date p_compdate) {
	if (strlen(p_student) != 6) {
		return 0;
	}
  char *asd = malloc(256);
	strcpy(asd, p_course);
	strcpy(or ->student, p_student);
  or ->course = asd;
	or ->grade = p_grade;
	or ->compdate = p_compdate;
	return 1;
}

struct oodi *add_record(struct oodi *array, unsigned int size, struct oodi newrec) {
	if (size == 0) {
     array = (struct oodi*) malloc(size * sizeof(struct oodi));

  }

  array = (struct oodi*) realloc(array, (size + 1) * sizeof(struct oodi));
	array[size] = newrec;
  array[size].course = malloc(strlen(newrec.course)+1);
  strcpy(array[size].course, newrec.course);
  //printf("%s\n", array[size].course);
  return array;
}

int change_grade(struct oodi *array, unsigned int size, const char *p_student, const char *p_course, unsigned char newgrade, struct date newdate) {
  int j = 0;
  int k = 0;
  while (j < size) {
    //printf("%s %s\n", p_student, p_course);
    //printf("%s %s\n----\n", array[j].student, array[j].course);
    //printf("%d\n", strcmp(array[j].student, p_student));
    if (strcmp(array[j].student, p_student) != 0 || strcmp(array[j].course, p_course) != 0) {
      k++;
	  }
    if (strcmp(array[j].student, p_student) == 0 && strcmp(array[j].course, p_course) == 0) {
      k = 0;
      break;
    }
    j++;
  }
  if (k == j) {
    return 0;
  }
	array[j].grade = newgrade;
	array[j].compdate = newdate;
	return 1;
}

/*int delete_array(struct oodi *array, unsigned int size) {
	return 1;
}*/
