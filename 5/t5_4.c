#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int *join_arrays(unsigned int num1, int *arr1, unsigned int num2, int *arr2, unsigned int num3, int *arr3) {
	int *ret = malloc((num1 + num2 + num3) * 4);
	memcpy(ret, arr1, 4*num1);
	ret = ret + num1;
	memcpy(ret, arr2, 4 * num2);
	ret = ret + num2;
	memcpy(ret, arr3, 4 * num3);
	ret = ret - num2 - num1;
	return ret;
}

int main(void)
{
	/* testing exercise. Feel free to modify */
	int a1[] = { 1, 2, 3, 4, 5 };
	int a2[] = { 10, 11, 12, 13, 14, 15, 16, 17 };
	int a3[] = { 20, 21, 22 };

	int *joined = join_arrays(5, a1, 8, a2, 3, a3);

	for (int i = 0; i < 5 + 8 + 3; i++) {
		printf("%d  ", joined[i]);
	}
	printf("\n");

	return 0;
}
