#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char *remove_comments(char *input) {
	char *save = input;
	int len = strlen(input);
	char *ret = malloc(len+1);
	memcpy(ret, input, len);
	int i = 0;
	int a = 0;
	int k = 0;


	while (*input) {
		char *tmp1 = strstr(input, "//");
		char *tmp2 = strstr(input, "/*");

		if (tmp1 == input) {

			while (*input != '\n') {
				input++;
				k++;
				i++;
			}
			input++;
			i++;
			k++;
			continue;
		}

		else if (tmp2 == input) {

			while (!(*input == '*' && *(input + 1) == '/')) {
				input++;
				k++;
				i++;
			}
			i = i + 2;
			k = k + 2;
			input = input + 2;
			continue;
		}

		else
		   ret[a] = *input;
		   a++;
		   i++;
		   input++;
   }
   input = save;

   int asd = strlen(input) - k;
   ret = realloc(ret, asd+1);
   ret[asd] = '\0';
   free(input);
   return ret;
}

char *read_file(const char *filename)
{
	FILE *f = fopen(filename, "r");
	if (!f)
		return NULL;

	char *buf = NULL;
	unsigned int count = 0;
	const unsigned int ReadBlock = 100;
	unsigned int n;
	do {
		buf = realloc(buf, count + ReadBlock + 1);
		n = fread(buf + count, 1, ReadBlock, f);
		count += n;
	} while (n == ReadBlock);

	buf[count] = 0;

	return buf;
}

int main(void)
{
	char *code = read_file("testifile.c");
	if (!code) {
		printf("No code read");
		return -1;
	}
	printf("-- Original:\n");
	fputs(code, stdout);

	code = remove_comments(code);
	printf("-- Comments removed:\n");
	fputs(code, stdout);

	free(code);
	return 0;
}
