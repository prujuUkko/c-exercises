#include <stdio.h>

int draw_triangle(unsigned int size) {
	int i = size;
	while (i > 0) {

		for (int a = i; a > 1; a--) {
			printf(".");
		}

		for (int b = size - i; b > 0; b--) {
			printf("#");
		}

		printf("#\n");
		i--;
	}

	return 0;
}

int main(void) {
    unsigned int a;
	scanf("%u", &a);
	int k = draw_triangle(a);
	return k;
}
