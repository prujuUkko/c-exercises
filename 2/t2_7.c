#include <stdio.h>
#include <ctype.h>

void ascii_chart(char min, char max) {
	int k = 1;

	for (int i = min; i <= max; i++) {

		int r = isprint(i);

		printf("%3d ", i);
		printf("0x%02x ", i);

        if (r == 0) {
			printf("?");


		  if (k % 4 == 0) {
				printf("\n");
			}

		  else
				printf("\t");
		}

		if (r != 0) {
			printf("%1c", i);

			if (k % 4 == 0) {
				printf("\n");
			}

			else
				printf("\t");
       }
	   k++;
	}
}

int main(void) {
	ascii_chart(28, 38);
	return 0;
}
