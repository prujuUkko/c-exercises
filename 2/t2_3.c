#include <stdio.h>

int simple_multiply(void)
{
  int a;
  int b;


  int ab = scanf("%d %d", &a, &b);
  if (ab >= 2) {
    int pro = a * b;
    printf("%d * %d = %d\n", a, b, pro);

  }
  return 0;
}

int simple_math(void)
{
  float a;
  float c;
  char b;


  int abc = scanf("%f %c %f", &a, &b, &c);
  if (abc >= 3) {
    switch(b) {

    case '+': {
      double d = a + c;
      printf("%.1f", d);
      break;
    }

    case '-': {
      double d = a - c;
      printf("%.1f", d);
      break;
    }

    case '*': {
      double d = a * c;
      printf("%.1f", d);
      break;
    }

    case '/': {
      double d = a / c;
      printf("%.1f\n", d);
      break;
    }

    default: {
      printf("ERR");
      break;
    }
    }
 }
  else {
    printf("ERR");
    }
  return 0;
}


int main(void)
{
  simple_multiply();
  simple_math();
  return 0;
