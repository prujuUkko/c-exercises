#include <stdio.h>
#include <string.h>
#include <stdlib.h>
struct nation {
	char name[21];
	unsigned int gold;
	unsigned int silver;
	unsigned int bronze;
};
//Sorts the nations 
int compare(const void *l, const void *r) {
	int ret;
	unsigned int g1 = ((struct nation *)l)->gold;
	unsigned int g2 = ((struct nation *)r)->gold;
	unsigned int s1 = ((struct nation *)l)->silver;
	unsigned int s2 = ((struct nation *)r)->silver;
	unsigned int b1 = ((struct nation *)l)->bronze;
	unsigned int b2 = ((struct nation *)r)->bronze;
	if (g1 == g2) {
		if (s1 == s2) {
			if (b1 == b2) {
				ret = 1;
			}
			if (b1 > b2) {
				ret = -1;
			}
			if (b2 > b1) {
				ret = 1;
			}
		}
		if (s1 > s2) {
			ret = -1;
		}
		if (s2 > s1) {
			ret = 1;
		}
	}
	if (g1 > g2) {
		ret = -1;
	}
	if (g2 > g1) {
		ret = 1;
	}
	return ret;
}
//Returns the amount of nations in the medal table, the last member is always a nation with an empty name and 0 medals
int arr_size(struct nation *arr) {
	int ret = 0;
	while (strcmp(arr[ret].name, " ") != 0) {
		ret++;
	}
	ret++;
	return ret;
}
//Initializes the medal table, the last member is a nation with an empty name and 0 medals 
struct nation *initialize_array() {
	struct nation *arr = calloc(1, sizeof(struct nation));
	strcpy(arr[0].name, " ");
	arr[0].gold = 0;
	arr[0].silver = 0;
	arr[0].bronze = 0;
	return arr;
}
//Adds a new nation to the medal table 
struct nation *add_nation(struct nation *arr, char *name) {
	int size = arr_size(arr);
	struct nation *tmp = arr;
	while (strcmp(arr->name, " ") != 0) {
		if (strcmp(arr->name, name) == 0) {
			printf("%s is already in the medal table\n", name);
			return arr;
		}
		arr++;
	}
	arr = tmp;
	struct nation *ret = realloc(arr, (size + 1) * sizeof(struct nation));
	strncpy(ret[size - 1].name, name, 20);
	ret[size - 1].gold = 0;
	ret[size - 1].silver = 0;
	ret[size - 1].bronze = 0;
	strcpy(ret[size].name, " ");
	ret[size].gold = 0;
	ret[size].silver = 0;
	ret[size].bronze = 0;
	return ret;
}
//Adds the amount of medals given in gold, silver and brozne to the nation given in name 
struct nation *update_medals(struct nation *arr, char *name, unsigned int gold, unsigned int silver, unsigned int bronze) {
	int i = 0;
	int trigger = 0;
	while (strcmp(arr[i].name, " ") != 0) {
		if (strcmp(arr[i].name, name) == 0) {
			trigger = 1;
			break;
		}
		i++;
	}
	if (trigger != 1) {
		printf("%s not found in the medal table\n", name);
		return arr;
	}
	arr[i].gold = arr[i].gold + gold;
	arr[i].silver = arr[i].silver + silver;
	arr[i].bronze = arr[i].bronze + bronze;
	return arr;
}
//Sorts and prints the medal table 
void output_medal_table(struct nation *arr) {
	int size = arr_size(arr);
	qsort(arr, size-1, sizeof(struct nation), compare);
	int i = 0;
	while (i < size - 1) {
		printf("%s %d %d %d\n", arr[i].name, arr[i].gold, arr[i].silver, arr[i].bronze);
		i++;
	}
}
//Saves the medal table to a .txt file given in name 
void save_table(struct nation *arr, char *name) {
	char tmp[256];
	strcpy(tmp, name);
	strcat(tmp, ".txt");
	FILE *f = fopen(tmp, "w");
	if (strcmp(arr[0].name, " ") == 0) {
		exit(0);
	}
	int size = arr_size(arr);
	int i = 0;
	while (i < size - 1) {
		fprintf(f, "%s %d %d %d\n", arr[i].name, arr[i].gold, arr[i].silver, arr[i].bronze);
		i++;
	}
	free(arr);
	exit(0);
}
//Loads a medal table from given file 
struct nation *load_table(struct nation *arr, char *filename) {
	if (strstr(filename, ".txt") == NULL) {
		printf("File %s not found\n", filename);
		return arr;
	}
	
	free(arr);
	struct nation *ret = initialize_array();
	FILE *f = fopen(filename, "r");
	char tmp[40];
	int gold;
	int silver;
	int bronze;
	char name[20];
	int scn;
	int i = 0;
	while (1) {
		if (feof(f) != 0) {
			break;
		}
		fgets(tmp, 40, f);
		scn = sscanf(tmp, "%s %d %d %d", name, &gold, &silver, &bronze);
		if (scn == 4) {
			ret = realloc(ret, sizeof(struct nation) * (i + 1));
			strncpy(ret[i].name, name, 20);
			ret[i].gold = gold;
			ret[i].silver = silver;
			ret[i].bronze = bronze;
			i++;
		}
		else {
			continue;
		}
	}
	
	fclose(f);
	strncpy(ret[i-1].name, " ", 20);
	ret[i-1].gold = 0;
	ret[i-1].silver = 0;
	ret[i-1].bronze = 0;
	return ret;
}
int main() {
	struct nation *arr = initialize_array();
	char str[256] = { 0 };
	char command = ' ';
	char name[256] = { 0 };
	unsigned int gold = 0;
	unsigned int silver = 0;
	unsigned int bronze = 0;
	int scn = 0;
	printf("To quit type: Q\n");
	while (1) {
		fgets(str, 256, stdin);
		scn = sscanf(str, " %c %s %d %d %d", &command, name, &gold, &silver, &bronze);
		if (feof(stdin)) {
			free(arr);
			exit(0);
		}
		if (command == 'A' && scn == 2) {
			arr = add_nation(arr, name);
		}
		else if (command == 'M' && scn == 5) {
			arr = update_medals(arr, name, gold, silver, bronze);
		}
		else if (command == 'L' && scn == 1) {
			output_medal_table(arr);
		}
		else if (command == 'W' && scn == 2) {
			save_table(arr, name);
		}
		else if (command == 'O' && scn == 2) {
			arr = load_table(arr, name);
		}
		else if (command == 'Q' && scn == 1) {
			free(arr);
			exit(0);
		}
		else {
			printf("Incorrect input\n");
		}
	}
	return 0;
}
