/* monster.c -- Implementation of monster actions
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "dungeon.h"

// for defining some monster types below that can be used in the game
typedef struct {
    char name[20];  // Name of monster
    char sign;  // character to show it on map
    unsigned int hplow;  // lowest possible initial maxhp
    unsigned int hphigh;  // highest possible initial maxhp
} MonstType;

// Specifying three monster types to start with.
// Feel free to add more, or change the below
// Note that it is up to you to decide whether to use this array from createMonsters
// you may or may not use it
const MonstType types[] = {
    { "Goblin", 'G', 6, 10},
    { "Rat", 'R', 3, 5},
    { "Dragon", 'D', 15, 20}
};


/* One kind of attack done by monster.
 * The attack function pointer can refer to this.
 *
 * Parameters:
 * game: the game state
 * monst: The monster performing attack
 */

 int checkMonster2(Game *game, int x, int y)
 {
     Creature *monst = game->monsters;
     for (unsigned int i = 0; i < game->numMonsters; i++) {
         if (monst[i].pos.x == x && monst[i].pos.y == y) {
             return 1;
         }
     }
     return 0;
 }

void attackPunch(Game *game, Creature *monst) {
    printf("%s punches you! ", monst->name);
    int hitprob = 50;
    int maxdam = 4;
    if (rand() % 100 < hitprob) {
        printf("Hit! ");
        int dam = rand() % maxdam + 1;
        printf("Damage: %d ", dam);
        game->hp = game->hp - dam;
        if (game->hp <= 0)
            printf("You died!");
        printf("\n");
    } else {
        printf("Miss!\n");
    }
}


/*
 * Move monster 'monst' towards the player character.
 * See exercise description for more detailed rules.
 */
void moveTowards(Game *game, Creature *monst) {
  int goal_x;
  int goal_y;

  if (game->position.x > monst->pos.x) {
    goal_x = monst->pos.x + 1;
  }

  if (game->position.x == monst->pos.x) {
    goal_x = monst->pos.x;
  }

  if (game->position.x < monst->pos.x) {
    goal_x = monst->pos.x - 1;
  }

  if (game->position.y > monst->pos.y) {
    goal_y = monst->pos.y + 1;
  }

  if (game->position.y == monst->pos.y) {
    goal_y = monst->pos.y;
  }

  if (game->position.y < monst->pos.y) {
    goal_y = monst->pos.y - 1;
  }

  printf("%d %d\n", goal_x, goal_y);

  if(isBlocked(game, goal_x, goal_y) == 0 && checkMonster2(game, goal_x, goal_y) == 0 && (goal_x != game->position.x || goal_y != game->position.y)) {
    monst->pos.x = goal_x;
    monst->pos.y = goal_y;
  }

  else if(isBlocked(game, monst->pos.x, goal_y) == 0 && checkMonster2(game, monst->pos.x, goal_y) == 0 && (monst->pos.x != game->position.x || goal_y != game->position.y)) {
    monst->pos.y = goal_y;
  }

  else if(isBlocked(game, goal_x, monst->pos.y) == 0 && checkMonster2(game, goal_x, monst->pos.y) == 0 && (goal_x != game->position.x || monst->pos.y != game->position.y)) {
    monst->pos.x = goal_x;
  }
}


/*
 * Move monster 'monst' away from the player character.
 * See exercise description for more detailed rules.
 */
void moveAway(Game *game, Creature *monst) {
  int goal_x;
  int goal_y;

  if (game->position.x > monst->pos.x) {
    goal_x = monst->pos.x - 1;
  }

  if (game->position.x == monst->pos.x) {
    goal_x = monst->pos.x;
  }

  if (game->position.x < monst->pos.x) {
    goal_x = monst->pos.x + 1;
  }

  if (game->position.y > monst->pos.y) {
    goal_y = monst->pos.y - 1;
  }

  if (game->position.y == monst->pos.y) {
    goal_y = monst->pos.y;
  }

  if (game->position.y < monst->pos.y) {
    goal_y = monst->pos.y + 1;
  }

  printf("%d %d\n", goal_x, goal_y);

  if(isBlocked(game, goal_x, goal_y) == 0 && checkMonster2(game, goal_x, goal_y) == 0 && (goal_x != game->position.x || goal_y != game->position.y)) {
    monst->pos.x = goal_x;
    monst->pos.y = goal_y;
  }

  else if(isBlocked(game, monst->pos.x, goal_y) == 0 && checkMonster2(game, monst->pos.x, goal_y) == 0 && (monst->pos.x != game->position.x || goal_y != game->position.y)) {
    monst->pos.y = goal_y;
  }

  else if(isBlocked(game, goal_x, monst->pos.y) == 0 && checkMonster2(game, goal_x, monst->pos.y) == 0 && (goal_x != game->position.x || monst->pos.y != game->position.y)) {
    monst->pos.x = goal_x;
  }
}

/*
 * Take action on each monster (that is alive) in 'monsters' array.
 * Each monster either attacks or moves (or does nothing if no action is specified)
 */
void monsterAction(Game *game) {
  unsigned int i = 0;
  int x_mons = 0;
  int y_mons = 0;
  int x_player = 0;
  int y_player = 0;
  int x_len = 0;
  int y_len = 0;
  while(i < game->opts.numMonsters) {
    x_mons = game->monsters[i].pos.x;
    y_mons = game->monsters[i].pos.y;
    x_player = game->position.x;
    y_player = game->position.y;

    x_len = x_player - x_mons;
    y_len = y_player - y_mons;

    if (x_len < 0) {
      x_len = x_len * (-1);
    }

    if (y_len < 0) {
      y_len = y_len * (-1);
    }

    if (y_len <= 1 && x_len <= 1) {
      game->monsters[i].attack(game, &game->monsters[i]);
    }

    else {
      game->monsters[i].move(game, &game->monsters[i]);
    }
    i++;
  }
  //printf("%d\n", game->numMonsters);
}


/*
 * Create opts.numMonsters monsters and position them on valid position
 * in the the game map. The moster data (hitpoints, name, map sign) should be
 * set appropriately (see exercise instructions)
 */
void createMonsters(Game *game) {
    unsigned int monst_count = 0;
    int i = 0;
    int r1;
    int r2;
    game->monsters = malloc(game->opts.numMonsters * sizeof(Creature));
    while (monst_count < game->opts.numMonsters) {
        r1 = rand() % game->opts.mapHeight;
        r2 = rand() % game->opts.mapWidth;

        if (isBlocked(game, r2, r1) == 0) {
            strncpy(game->monsters[i].name, "asd", 20);
            game->monsters[i].sign = 'D';
            game->monsters[i].pos.x = r2;
            game->monsters[i].pos.y = r1;
            game->monsters[i].hp = 20;
            game->monsters[i].maxhp = 20;
            game->monsters[i].move = moveTowards;
            game->monsters[i].attack = attackPunch;
            i++;
            monst_count++;
            game->numMonsters++;
        }

        else {
          continue;
        }
    }
}

/* Determine whether monster moves towards or away from player character.
 */
void checkIntent(Game *game)
{
    for (unsigned int i = 0; i < game->numMonsters; i++) {
        Creature *m = &game->monsters[i];
        if (m->hp <= 2) {
            m->move = moveAway;
        } else {
            m->move = moveTowards;
        }
        if (m->hp < m->maxhp)
            m->hp = m->hp + 0.1;  // heals a bit every turn
    }
}
