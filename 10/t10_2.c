#include <stdio.h>
#include <stdarg.h>
#include <string.h>

int myprint(const char *str, ...) {
	const char *tmp;
	int i;
	int ret = 0;
	va_list args;
	va_start(args, str);


	while (*str != '\0') {
		tmp = strchr(str, '&');

		if (tmp == str) {
			i = va_arg(args, int);
			printf("%d", i);
			ret++;
		}

		else {
			fputc(*str, stdout);
		}

		str++;
	}
	va_end(args);
	return ret;
}
