#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "fpointers.h"

int compareLexical(const void* l, const void* r) {
	const char** tmp1;
	const char** tmp2;

	tmp1 = (const char **)l;
	tmp2 = (const char **)r;
	int ret = strcmp(*tmp1, *tmp2);
	return ret;
}

int compareNumerical(const void* l, const void* r) {
	int ret;
	int i = 0;
	int j = 0;

	const char** tmp1;
	const char** tmp2;

	tmp1 = (const char **)l;
	tmp2 = (const char **)r;

	printf("%s / %s\n", *tmp1, *tmp2);

	sscanf(*tmp1, " %d", &i);
	sscanf(*tmp2, " %d", &j);

	printf("%d %d\n", i, j);

	if (i == j) {
		ret = 0;
	}

	if (i > j) {
		ret = 1;
	}

	if (j > i) {
		ret = -1;
	}

	return ret;
}

void sortStrings(char **strings, int(*cmp)(const void*, const void*)) {
	int len = 0;
	char **tmp;
	tmp = strings;
	while (*strings != NULL) {
		len++;
		strings++;
	}
	strings = tmp;

	qsort(strings, len, sizeof(char *), cmp);
}

char** findString(char **strings, const char *str, int(*cmp)(const void*, const void*)) {
	int len = 0;
	char **tmp;
	tmp = strings;
	while (*strings != NULL) {
		len++;
		strings++;
	}
	strings = tmp;

  char **ret;
	ret = (char **) bsearch(&str, strings, len, sizeof(char *), cmp);
	return ret;

}
